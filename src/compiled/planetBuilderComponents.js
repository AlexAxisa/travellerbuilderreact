"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var PlanetWrapper = (function (_super) {
    __extends(PlanetWrapper, _super);
    function PlanetWrapper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PlanetWrapper.prototype.render = function () {
        return (React.createElement("div", { className: "PlanetWrapper" },
            React.createElement(PlanetDetails, null)));
    };
    return PlanetWrapper;
}(React.Component));
exports.PlanetWrapper = PlanetWrapper;
var PlanetDetails = (function (_super) {
    __extends(PlanetDetails, _super);
    function PlanetDetails() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PlanetDetails.prototype.render = function () {
        return (React.createElement("div", null,
            "Planet Details",
            React.createElement(GenerateButtions, null),
            React.createElement(CurrentPlanet, null)));
    };
    return PlanetDetails;
}(React.Component));
var GenerateButtions = (function (_super) {
    __extends(GenerateButtions, _super);
    function GenerateButtions() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GenerateButtions.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(GenerateSubsector, null),
            React.createElement(GeneratePlanetButton, null),
            React.createElement(CreateEmptyPlanet, null)));
    };
    return GenerateButtions;
}(React.Component));
var GenerateSubsector = (function (_super) {
    __extends(GenerateSubsector, _super);
    function GenerateSubsector() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GenerateSubsector.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("button", { id: "GenerateSubsector" }, "Generate Subsector")));
    };
    return GenerateSubsector;
}(React.Component));
var GeneratePlanetButton = (function (_super) {
    __extends(GeneratePlanetButton, _super);
    function GeneratePlanetButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GeneratePlanetButton.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("button", { id: "GeneratePlanetButton" }, "Generate Planet")));
    };
    return GeneratePlanetButton;
}(React.Component));
var CreateEmptyPlanet = (function (_super) {
    __extends(CreateEmptyPlanet, _super);
    function CreateEmptyPlanet() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CreateEmptyPlanet.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("button", { id: "CreateEmptyPlanet" }, "Create Empty Planet")));
    };
    return CreateEmptyPlanet;
}(React.Component));
var CurrentPlanet = (function (_super) {
    __extends(CurrentPlanet, _super);
    function CurrentPlanet() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CurrentPlanet.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(PlanetName, null)));
    };
    return CurrentPlanet;
}(React.Component));
var PlanetName = (function (_super) {
    __extends(PlanetName, _super);
    function PlanetName() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PlanetName.prototype.render = function () {
        return (React.createElement("div", null,
            "Planet Name: ",
            React.createElement("input", { type: "text" })));
    };
    return PlanetName;
}(React.Component));
//# sourceMappingURL=planetBuilderComponents.js.map