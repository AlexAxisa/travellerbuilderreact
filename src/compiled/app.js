"use strict";
exports.__esModule = true;
var React = require("react");
var ReactDOM = require("react-dom");
require("./index.css");
var PlanetComponents = require("./planetBuilderComponents/planetBuilderBase");
var planetContext_1 = require("./planetContext");
fetch("http://localhost:3000/PlanetInformation.json")
    .then(function (response) { return response.json(); })
    .then(function (data) {
    ReactDOM.render(React.createElement(planetContext_1.PlanetInformationProvider, { json: data },
        React.createElement(PlanetComponents.PlanetWrapper, null)), document.getElementById("root"));
});
//# sourceMappingURL=app.js.map