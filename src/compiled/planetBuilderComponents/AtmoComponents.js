"use strict";
exports.__esModule = true;
var React = require("react");
function AtmoWrapper() {
    return (React.createElement("div", null,
        React.createElement(AtmoMessage, null),
        React.createElement(Atmo, null)));
}
exports.AtmoWrapper = AtmoWrapper;
function AtmoMessage() {
    return React.createElement("div", { id: "AtmoMessage" });
}
function Atmo() {
    return (React.createElement("div", null,
        React.createElement(AtmoForm, null),
        React.createElement(AtmoTable, null)));
}
function AtmoForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "AtmoInput", type: "number" }),
            React.createElement("input", { id: "AtmoSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function AtmoTable() {
    return (React.createElement("table", { id: "AtmoTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Atmosphere Value"),
            React.createElement("th", null, "Composition"),
            React.createElement("th", null, "Examples"),
            React.createElement("th", null, "GearRequired"),
            React.createElement("th", null, "MinTL"))));
}
//# sourceMappingURL=atmoComponents.js.map