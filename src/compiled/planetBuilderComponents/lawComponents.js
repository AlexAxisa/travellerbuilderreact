"use strict";
exports.__esModule = true;
var React = require("react");
function LawWrapper() {
    return (React.createElement("div", null,
        React.createElement(LawMessage, null),
        React.createElement(Law, null)));
}
exports.LawWrapper = LawWrapper;
function LawMessage() {
    return React.createElement("div", { id: "LawMessage" });
}
function Law() {
    return (React.createElement("div", null,
        React.createElement(LawForm, null),
        React.createElement(LawTable, null)));
}
function LawForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "LawInput", type: "number" }),
            React.createElement("input", { id: "LawSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function LawTable() {
    return (React.createElement("table", { id: "LawTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Law Value"),
            React.createElement("th", null, "Weapons"),
            React.createElement("th", null, "Armour"))));
}
//# sourceMappingURL=lawComponents.js.map