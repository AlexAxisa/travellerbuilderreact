"use strict";
exports.__esModule = true;
var React = require("react");
function HydroWrapper() {
    return (React.createElement("div", null,
        React.createElement(HydroMessage, null),
        React.createElement(Hydro, null)));
}
exports.HydroWrapper = HydroWrapper;
function HydroMessage() {
    return React.createElement("div", { id: "HydroMessage" });
}
function Hydro() {
    return (React.createElement("div", null,
        React.createElement(HydroForm, null),
        React.createElement(HydroTable, null)));
}
function HydroForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "HydroInput", type: "number" }),
            React.createElement("input", { id: "HydroSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function HydroTable() {
    return (React.createElement("table", { id: "HydroTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Hydrographics Value"),
            React.createElement("th", null, "Percentage"),
            React.createElement("th", null, "Description"))));
}
//# sourceMappingURL=hydroComponents.js.map