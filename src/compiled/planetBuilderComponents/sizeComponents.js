"use strict";
exports.__esModule = true;
var React = require("react");
var planetContext_1 = require("../planetContext");
function SizeWrapper() {
    return (React.createElement("div", null,
        React.createElement(SizeMessage, null),
        React.createElement(Size, null)));
}
exports.SizeWrapper = SizeWrapper;
function SizeMessage() {
    return React.createElement("div", { id: "SizeMessage" });
}
function Size() {
    return (React.createElement("div", null,
        React.createElement(SizeForm, null),
        React.createElement(SizeTable, null)));
}
function SizeForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "SizeInput", type: "number" }),
            React.createElement("input", { id: "SizeSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function SizeTable() {
    return (React.createElement("table", { id: "SizeTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Size Value"),
            React.createElement("th", null, "Diameter"),
            React.createElement("th", null, "Example"),
            React.createElement("th", null, "Surface Gravity")),
        React.createElement("tr", null,
            React.createElement(SizeTableEntriesTesting, null))));
}
function SizeTableEntriesTesting() {
    var _a = React.useContext(planetContext_1.PlanetContext), context = _a[0], setcontext = _a[1];
    var rows = [];
    for (var i = 0; i < context.sizes.size; i++) {
        var row = React.createElement("tr", null, context.sizes.get(i).example);
        rows.push(row);
    }
    return React.createElement(React.Fragment, null, rows);
}
//# sourceMappingURL=sizeComponents.js.map