"use strict";
exports.__esModule = true;
var React = require("react");
function TradeWrapper() {
    return (React.createElement("div", null,
        React.createElement(TradeMessage, null),
        React.createElement(Trade, null)));
}
exports.TradeWrapper = TradeWrapper;
function TradeMessage() {
    return React.createElement("div", { id: "TradeMessage" });
}
function Trade() {
    return (React.createElement("div", null,
        React.createElement(TradeForm, null),
        React.createElement(TradeTable, null)));
}
function TradeForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "TradeInput", type: "number" }),
            React.createElement("input", { id: "TradeSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function TradeTable() {
    return (React.createElement("table", { id: "TradeTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Classification"),
            React.createElement("th", null, "Code"))));
}
//# sourceMappingURL=tradecodeComponents.js.map