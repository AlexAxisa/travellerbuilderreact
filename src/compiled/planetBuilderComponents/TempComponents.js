"use strict";
exports.__esModule = true;
var React = require("react");
function TempWrapper() {
    return (React.createElement("div", null,
        React.createElement(TempMessage, null),
        React.createElement(Temp, null)));
}
exports.TempWrapper = TempWrapper;
function TempMessage() {
    return React.createElement("div", { id: "TempMessage" });
}
function Temp() {
    return (React.createElement("div", null,
        React.createElement(TempForm, null),
        React.createElement(TempTable, null)));
}
function TempForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "TempInput", type: "number" }),
            React.createElement("input", { id: "TempSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function TempTable() {
    return (React.createElement("table", { id: "TempTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Temperature Value"),
            React.createElement("th", null, "Type"),
            React.createElement("th", null, "Average Tempature"),
            React.createElement("th", null, "Description"))));
}
//# sourceMappingURL=TempComponents.js.map