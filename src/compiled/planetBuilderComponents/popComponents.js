"use strict";
exports.__esModule = true;
var React = require("react");
function PopWrapper() {
    return (React.createElement("div", null,
        React.createElement(PopMessage, null),
        React.createElement(Pop, null)));
}
exports.PopWrapper = PopWrapper;
function PopMessage() {
    return React.createElement("div", { id: "PopMessage" });
}
function Pop() {
    return (React.createElement("div", null,
        React.createElement(PopForm, null),
        React.createElement(PopTable, null)));
}
function PopForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "PopInput", type: "number" }),
            React.createElement("input", { id: "PopSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function PopTable() {
    return (React.createElement("table", { id: "PopTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Population Value"),
            React.createElement("th", null, "Inhabitants"),
            React.createElement("th", null, "Range"),
            React.createElement("th", null, "Description"))));
}
//# sourceMappingURL=popComponents.js.map