"use strict";
exports.__esModule = true;
var React = require("react");
function GovWrapper() {
    return (React.createElement("div", null,
        React.createElement(GovMessage, null),
        React.createElement(Gov, null)));
}
exports.GovWrapper = GovWrapper;
function GovMessage() {
    return React.createElement("div", { id: "GovMessage" });
}
function Gov() {
    return (React.createElement("div", null,
        React.createElement(GovForm, null),
        React.createElement(GovTable, null)));
}
function GovForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "GovInput", type: "number" }),
            React.createElement("input", { id: "GovSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function GovTable() {
    return (React.createElement("table", { id: "GovTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Government Value"),
            React.createElement("th", null, "Type"),
            React.createElement("th", null, "Description"),
            React.createElement("th", null, "Examples"),
            React.createElement("th", null, "Contraband"))));
}
//# sourceMappingURL=govComponents.js.map