"use strict";
exports.__esModule = true;
var React = require("react");
var atmoComponents_1 = require("./atmoComponents");
var govComponents_1 = require("./govComponents");
var hydroComponents_1 = require("./hydroComponents");
var lawComponents_1 = require("./lawComponents");
var popComponents_1 = require("./popComponents");
var sizeComponents_1 = require("./sizeComponents");
var starportComponents_1 = require("./starportComponents");
var TempComponents_1 = require("./TempComponents");
var tradecodeComponents_1 = require("./tradecodeComponents");
function PlanetWrapper() {
    return (React.createElement("div", { className: "PlanetWrapper" },
        React.createElement(PlanetDetails, null)));
}
exports.PlanetWrapper = PlanetWrapper;
function PlanetDetails() {
    return (React.createElement("div", null,
        "Planet Details",
        React.createElement(GenerateButtions, null),
        React.createElement(CurrentPlanet, null),
        React.createElement(sizeComponents_1.SizeWrapper, null),
        React.createElement(atmoComponents_1.AtmoWrapper, null),
        React.createElement(TempComponents_1.TempWrapper, null),
        React.createElement(hydroComponents_1.HydroWrapper, null),
        React.createElement(popComponents_1.PopWrapper, null),
        React.createElement(govComponents_1.GovWrapper, null),
        React.createElement(lawComponents_1.LawWrapper, null),
        React.createElement(starportComponents_1.StarportWrapper, null),
        React.createElement(tradecodeComponents_1.TradeWrapper, null)));
}
function GenerateButtions() {
    return (React.createElement("div", null,
        React.createElement(GenerateSubsector, null),
        React.createElement(GeneratePlanetButton, null),
        React.createElement(CreateEmptyPlanet, null)));
}
function GenerateSubsector() {
    return (React.createElement("div", null,
        React.createElement("button", { id: "GenerateSubsector" }, "Generate Subsector")));
}
function GeneratePlanetButton() {
    return (React.createElement("div", null,
        React.createElement("button", { id: "GeneratePlanetButton" }, "Generate Planet")));
}
function CreateEmptyPlanet() {
    return (React.createElement("div", null,
        React.createElement("button", { id: "CreateEmptyPlanet" }, "Create Empty Planet")));
}
function CurrentPlanet() {
    return (React.createElement("div", null,
        React.createElement(PlanetName, null)));
}
function PlanetName() {
    return (React.createElement("div", null,
        "Planet Name: ",
        React.createElement("input", { type: "text" })));
}
//# sourceMappingURL=planetBuilderBase.js.map