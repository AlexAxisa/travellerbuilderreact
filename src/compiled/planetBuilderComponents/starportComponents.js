"use strict";
exports.__esModule = true;
var React = require("react");
function StarportWrapper() {
    return (React.createElement("div", null,
        React.createElement(StarportMessage, null),
        React.createElement(Starport, null)));
}
exports.StarportWrapper = StarportWrapper;
function StarportMessage() {
    return React.createElement("div", { id: "StarportMessage" });
}
function Starport() {
    return (React.createElement("div", null,
        React.createElement(StarportForm, null),
        React.createElement(StarportTable, null)));
}
function StarportForm() {
    return (React.createElement("div", null,
        React.createElement("form", null,
            React.createElement("input", { id: "StarportInput", type: "number" }),
            React.createElement("input", { id: "StarportSubmitButton", type: "submit", value: "Submit", disabled: true }))));
}
function StarportTable() {
    return (React.createElement("table", { id: "StarportTable" },
        React.createElement("tr", null,
            React.createElement("th", null, "Class"),
            React.createElement("th", null, "Quality"),
            React.createElement("th", null, "Berthing"),
            React.createElement("th", null, "Fuel"),
            React.createElement("th", null, "Facilities"),
            React.createElement("th", null, "Naval"),
            React.createElement("th", null, "Scout"),
            React.createElement("th", null, "Research"),
            React.createElement("th", null, "Tas"))));
}
//# sourceMappingURL=starportComponents.js.map