"use strict";
exports.__esModule = true;
function importPlanetInformation(json) {
    var sizes = new Map();
    var minSize = Number.POSITIVE_INFINITY;
    var maxSize = Number.NEGATIVE_INFINITY;
    json.Size.forEach(function (currentSize) {
        if (currentSize.Size < minSize) {
            minSize = currentSize.Size;
        }
        if (currentSize.Size > maxSize) {
            maxSize = currentSize.Size;
        }
        var nextSize = {
            diameter: currentSize.Diameter,
            example: currentSize.Example,
            surfaceGravity: currentSize.SurfaceGravity,
            TLMod: currentSize.TLMod
        };
        sizes.set(currentSize.Size, nextSize);
    });
    var atmospheres = new Map();
    var minAtmo = Number.POSITIVE_INFINITY;
    var maxAtmo = Number.NEGATIVE_INFINITY;
    json.Atmosphere.forEach(function (currentAthmo) {
        if (currentAthmo.Atmosphere < minAtmo) {
            minAtmo = currentAthmo.Atmosphere;
        }
        if (currentAthmo.Atmosphere > maxAtmo) {
            maxAtmo = currentAthmo.Atmosphere;
        }
        var nextAthmo = {
            composition: currentAthmo.Composition,
            example: currentAthmo.Examples,
            pressureMin: currentAthmo.PressureMin,
            pressureMax: currentAthmo.PressureMax,
            gearRequired: currentAthmo.GearRequired,
            minTL: currentAthmo.MinTL,
            TLMod: currentAthmo.TLMod
        };
        atmospheres.set(currentAthmo.Atmosphere, nextAthmo);
    });
    var temperatures = new Map();
    var minTemp = Number.POSITIVE_INFINITY;
    var maxTemp = Number.NEGATIVE_INFINITY;
    json.Temperature.forEach(function (currentTemp) {
        if (currentTemp.Roll < minTemp) {
            minTemp = currentTemp.Roll;
        }
        if (currentTemp.Roll > maxTemp) {
            maxTemp = currentTemp.Roll;
        }
        var nextTemp = {
            type: currentTemp.Type,
            averageTemp: currentTemp.AverageTemp,
            description: currentTemp.Description
        };
        temperatures.set(currentTemp.Roll, nextTemp);
    });
    var hydrographics = new Map();
    var minHydro = Number.POSITIVE_INFINITY;
    var maxHydro = Number.NEGATIVE_INFINITY;
    json.Hydrographics.forEach(function (currentHydro) {
        if (currentHydro.Hydrographics < minHydro) {
            minHydro = currentHydro.Hydrographics;
        }
        if (currentHydro.Hydrographics > maxHydro) {
            maxHydro = currentHydro.Hydrographics;
        }
        var nextHydro = {
            percentage: currentHydro.Percentage,
            description: currentHydro.Description,
            TLMod: currentHydro.TLMod
        };
        hydrographics.set(currentHydro.Hydrographics, nextHydro);
    });
    var populations = new Map();
    var minPop = Number.POSITIVE_INFINITY;
    var maxPop = Number.NEGATIVE_INFINITY;
    json.Population.forEach(function (currentPop) {
        if (currentPop.Population < minPop) {
            minPop = currentPop.Population;
        }
        if (currentPop.Population > maxPop) {
            maxPop = currentPop.Population;
        }
        var nextPop = {
            inhabitants: currentPop.Inhabitants,
            range: currentPop.Range,
            description: currentPop.Description,
            TLMod: currentPop.TLMod
        };
        populations.set(currentPop.Population, nextPop);
    });
    var governments = new Map();
    var minGov = Number.POSITIVE_INFINITY;
    var maxGov = Number.NEGATIVE_INFINITY;
    json.Government.forEach(function (currentGov) {
        if (currentGov.Government < minGov) {
            minGov = currentGov.Government;
        }
        if (currentGov.Government > maxGov) {
            maxGov = currentGov.Government;
        }
        var nextGov = {
            TLMod: currentGov.TLMod,
            type: currentGov.Type,
            description: currentGov.Description,
            examples: currentGov.Examples,
            contraband: currentGov.Contraband
        };
        governments.set(currentGov.Government, nextGov);
    });
    var lawLevels = new Map();
    var minLaw = Number.POSITIVE_INFINITY;
    var maxLaw = Number.NEGATIVE_INFINITY;
    json.LawLevel.forEach(function (currentLaw) {
        if (currentLaw.Law < minLaw) {
            minLaw = currentLaw.Law;
        }
        if (currentLaw.Law > maxLaw) {
            maxLaw = currentLaw.Law;
        }
        var nextLaw = {
            weapons: currentLaw.Weapons,
            armor: currentLaw.Armour
        };
        lawLevels.set(currentLaw.Law, nextLaw);
    });
    var starportClasses = new Map();
    var minClass = Number.POSITIVE_INFINITY;
    var maxClass = Number.NEGATIVE_INFINITY;
    json.StarportClass.forEach(function (currentStarport) {
        if (currentStarport.Roll < minClass) {
            minClass = currentStarport.Roll;
        }
        if (currentStarport.Roll > maxClass) {
            maxClass = currentStarport.Roll;
        }
        var nextStarport = {
            "class": currentStarport.Class
        };
        starportClasses.set(currentStarport.Roll, nextStarport);
    });
    var starportFacilities = new Map();
    json.StarportFacilities.forEach(function (currentStarFac) {
        var nextStarFac = {
            TLMod: currentStarFac.TLMod,
            quality: currentStarFac.Quality,
            berthing: currentStarFac.Berthing,
            fuel: currentStarFac.Fuel,
            facilities: currentStarFac.Facilities,
            naval: currentStarFac.Naval,
            scout: currentStarFac.Scout,
            research: currentStarFac.Research,
            TAS: currentStarFac.Tas
        };
        starportFacilities.set(currentStarFac.Class, nextStarFac);
    });
    var tradeCodes = [];
    json.TradeCodes.forEach(function (currentCode) {
        var nextCode = {
            classification: currentCode.Classification,
            code: currentCode.Code,
            planetSizes: currentCode.PlanetSize,
            atmospheres: currentCode.Atmosphere,
            hydro: currentCode.Hydro,
            population: currentCode.Population,
            governments: currentCode.Government,
            lawLevels: currentCode.LawLevel,
            techLevels: currentCode.TechLevel
        };
        tradeCodes.push(nextCode);
    });
    var ranges = {
        minSize: minSize,
        maxSize: maxSize,
        minAtmo: minAtmo,
        maxAtmo: maxAtmo,
        minTemp: minTemp,
        maxTemp: maxTemp,
        minHydro: minHydro,
        maxHydro: maxHydro,
        minPop: minPop,
        maxPop: maxPop,
        minGov: minGov,
        maxGov: maxGov,
        minLaw: minLaw,
        maxLaw: maxLaw,
        minClass: minClass,
        maxClass: maxClass
    };
    var allInfo = {
        sizes: sizes,
        atmospheres: atmospheres,
        temperatures: temperatures,
        hydrographics: hydrographics,
        populations: populations,
        governments: governments,
        lawLevels: lawLevels,
        starportClasses: starportClasses,
        starportFacilities: starportFacilities,
        tradeCodes: tradeCodes,
        ranges: ranges
    };
    return allInfo;
}
exports.importPlanetInformation = importPlanetInformation;
//# sourceMappingURL=PlanetImport.js.map