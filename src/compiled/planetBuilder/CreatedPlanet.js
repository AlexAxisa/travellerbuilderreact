"use strict";
exports.__esModule = true;
var generalFunctions_1 = require("./generalFunctions");
var CreatedPlanet = (function () {
    function CreatedPlanet(sizeRoll) {
        if (sizeRoll === void 0) { sizeRoll = undefined; }
        this.sizeRoll = undefined;
        this.sizeValue = undefined;
        this.atmoRoll = undefined;
        this.atmoValue = undefined;
        this.tempRoll = undefined;
        this.tempValue = undefined;
        this.hydroRoll = undefined;
        this.hydroValue = undefined;
        this.popRoll = undefined;
        this.popValue = undefined;
        this.lawRoll = undefined;
        this.lawValue = undefined;
        this.classRoll = undefined;
        this.classValue = undefined;
        this.starportDetails = undefined;
        this.techLevel = undefined;
        this.tradeCodes = [];
    }
    CreatedPlanet.prototype.setPlanetSize = function (value, sizes, ranges) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(2) - 2;
            if (roll < ranges.minSize) {
                roll = ranges.minSize;
            }
            else if (roll > ranges.maxSize) {
                roll = ranges.maxSize;
            }
            value = roll;
        }
        this.sizeRoll = value;
        this.sizeValue = sizes.get(value);
    };
    CreatedPlanet.prototype.setPlanetAtmosphere = function (value, atmospheres, ranges) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(2) - 7 + this.sizeRoll;
            if (roll < ranges.minAtmo) {
                roll = ranges.minAtmo;
            }
            else if (roll > ranges.maxAtmo) {
                roll = ranges.maxAtmo;
            }
            value = roll;
        }
        this.atmoRoll = value;
        this.atmoValue = atmospheres.get(value);
    };
    CreatedPlanet.prototype.setPlanetTemperature = function (value, temperatures, ranges) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(2);
            switch (this.atmoRoll) {
                case 0:
                case 1:
                    roll = 0;
                    break;
                case 2:
                case 3:
                    roll = roll - 2;
                    break;
                case 4:
                case 5:
                case 14:
                    roll = roll - 1;
                    break;
                case 8:
                case 9:
                    roll = roll + 1;
                    break;
                case 10:
                case 13:
                case 15:
                    roll = roll + 2;
                    break;
                case 11:
                case 12:
                    roll = roll + 6;
                default:
                    break;
            }
            if (roll < ranges.minTemp) {
                roll = ranges.minTemp;
            }
            else if (roll > ranges.maxTemp) {
                roll = ranges.maxTemp;
            }
            value = roll;
        }
        this.tempRoll = value;
        var currentSegment = undefined;
        temperatures.forEach(function (mapValue, key) {
            if (value <= key &&
                (currentSegment > key || typeof currentSegment == "undefined")) {
                currentSegment = key;
            }
        });
        this.tempValue = temperatures.get(currentSegment);
    };
    CreatedPlanet.prototype.setPlanetHydro = function (value, hydrographics, ranges) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(2) - 7;
            if (this.sizeRoll == 1 || this.sizeRoll == 2) {
                roll = 0;
            }
            else if (this.atmoRoll == 0 ||
                this.atmoRoll == 1 ||
                this.atmoRoll == 10 ||
                this.atmoRoll == 11 ||
                this.atmoRoll == 12) {
                roll = roll - 4;
            }
            else {
                roll = roll + this.atmoRoll;
            }
            if (this.atmoRoll !== 13 && this.atmoRoll !== 15) {
                if (this.tempRoll == 11) {
                    roll = roll - 2;
                }
                else if (this.tempRoll == 12) {
                    roll = roll - 6;
                }
            }
            if (roll < ranges.minHydro) {
                roll = ranges.minHydro;
            }
            else if (roll > ranges.maxHydro) {
                roll = ranges.maxHydro;
            }
            value = roll;
        }
        this.hydroRoll = value;
        this.hydroValue = hydrographics.get(value);
    };
    CreatedPlanet.prototype.setPlanetPopulation = function (value, population, ranges) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(2) - 2;
            if (roll < ranges.minPop) {
                roll = ranges.minPop;
            }
            else if (roll > ranges.maxPop) {
                roll = ranges.maxPop;
            }
            value = roll;
        }
        this.popRoll = value;
        this.popValue = population.get(value);
    };
    CreatedPlanet.prototype.setPlanetGovernment = function (value, governments, ranges) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(2) - 7 + this.popRoll;
            if (this.popRoll === 0) {
                roll = 0;
            }
            if (roll < ranges.minGov) {
                roll = ranges.minGov;
            }
            else if (roll > ranges.maxGov) {
                roll = ranges.maxGov;
            }
            value = roll;
        }
        this.govRoll = value;
        this.govValue = governments.get(value);
    };
    CreatedPlanet.prototype.setPlanetFaction = function () { };
    CreatedPlanet.prototype.setPlanetLaw = function (value, laws, ranges) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(2) - 7 + this.govRoll;
            if (this.popRoll === 0) {
                roll = 0;
            }
            if (roll < ranges.minLaw) {
                roll = ranges.minLaw;
            }
            else if (roll > ranges.maxLaw) {
                roll = ranges.maxLaw;
            }
            value = roll;
        }
        this.lawRoll = value;
        this.lawValue = laws.get(value);
    };
    CreatedPlanet.prototype.setPlanetStarport = function (value, starport, ranges) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(2);
            if (this.popRoll > 10) {
                roll = roll + 2;
            }
            else if (this.popRoll > 8) {
                roll = roll + 1;
            }
            else if (this.popRoll < 2) {
                roll = roll - 2;
            }
            else if (this.popRoll < 4) {
                roll = roll - 1;
            }
            if (roll < ranges.minClass) {
                roll = ranges.minClass;
            }
            else if (roll > ranges.maxClass) {
                roll = ranges.maxClass;
            }
            value = roll;
        }
        this.classRoll = value;
        this.classValue = starport.get(value);
    };
    CreatedPlanet.prototype.setStarportDetails = function (starportFacilities) {
        var starportConstuctor = starportFacilities.get(this.classValue["class"]);
        var berthingCost = generalFunctions_1.rollDice(1) * starportConstuctor.berthing;
        var naval = generalFunctions_1.rollDice(2) > starportConstuctor.naval;
        var scout = generalFunctions_1.rollDice(2) > starportConstuctor.scout;
        var research = generalFunctions_1.rollDice(2) > starportConstuctor.research;
        var tas = generalFunctions_1.rollDice(2) > starportConstuctor.TAS;
        this.starportDetails = {
            TLMod: starportConstuctor.TLMod,
            quality: starportConstuctor.quality,
            facilities: starportConstuctor.facilities,
            fuel: starportConstuctor.fuel,
            berthingCost: berthingCost,
            naval: naval,
            scout: scout,
            research: research,
            TAS: tas
        };
    };
    CreatedPlanet.prototype.setTechLevel = function (value) {
        if (typeof value === "undefined") {
            var roll = generalFunctions_1.rollDice(1);
            roll += this.starportDetails.TLMod;
            roll += this.sizeValue.TLMod;
            roll += this.atmoValue.TLMod;
            roll += this.hydroValue.TLMod;
            roll += this.popValue.TLMod;
            roll += this.govValue.TLMod;
            if (this.popRoll === 0) {
                roll = 0;
            }
            value = roll;
        }
        this.techLevel = value;
    };
    CreatedPlanet.prototype.calcTradeCodes = function (codes) {
        var _this = this;
        this.tradeCodes = [];
        codes.forEach(function (nextCode) {
            var planetIsValid = chechValidCodeParamter(nextCode.planetSizes, _this.sizeRoll);
            var atmoIsValid = chechValidCodeParamter(nextCode.atmospheres, _this.atmoRoll);
            var hydroIsValid = chechValidCodeParamter(nextCode.hydro, _this.hydroRoll);
            var popIsValid = chechValidCodeParamter(nextCode.population, _this.popRoll);
            var lawIsValid = chechValidCodeParamter(nextCode.lawLevels, _this.lawRoll);
            var TechisValid = chechValidCodeParamter(nextCode.techLevels, _this.techLevel);
            if (planetIsValid &&
                atmoIsValid &&
                hydroIsValid &&
                popIsValid &&
                lawIsValid &&
                TechisValid) {
                _this.tradeCodes.push(nextCode);
            }
        });
    };
    return CreatedPlanet;
}());
exports.CreatedPlanet = CreatedPlanet;
function chechValidCodeParamter(possibleOptions, verifiedParamater) {
    if (possibleOptions.length !== 0) {
        var isValid_1 = false;
        possibleOptions.forEach(function (value) {
            if (value == verifiedParamater) {
                isValid_1 = true;
            }
        });
        return isValid_1;
    }
    else {
        return true;
    }
}
//# sourceMappingURL=CreatedPlanet.js.map