"use strict";
exports.__esModule = true;
var CreatedPlanet_1 = require("./CreatedPlanet");
function rollDice(dice) {
    var totalroll = 0;
    for (var i = 0; i < dice; i++) {
        totalroll += Math.floor(Math.random() * Math.floor(6)) + 1;
    }
    return totalroll;
}
exports.rollDice = rollDice;
function generatePlanet(info, display) {
    if (display === void 0) { display = true; }
    var inputValue = undefined;
    var generatedPlanet = new CreatedPlanet_1.CreatedPlanet();
    generatedPlanet.setPlanetSize(inputValue, info.sizes, info.ranges);
    generatedPlanet.setPlanetAtmosphere(inputValue, info.atmospheres, info.ranges);
    generatedPlanet.setPlanetTemperature(inputValue, info.temperatures, info.ranges);
    generatedPlanet.setPlanetHydro(inputValue, info.hydrographics, info.ranges);
    generatedPlanet.setPlanetPopulation(inputValue, info.populations, info.ranges);
    generatedPlanet.setPlanetGovernment(inputValue, info.governments, info.ranges);
    generatedPlanet.setPlanetLaw(inputValue, info.lawLevels, info.ranges);
    generatedPlanet.setPlanetStarport(inputValue, info.starportClasses, info.ranges);
    generatedPlanet.setStarportDetails(info.starportFacilities);
    generatedPlanet.setTechLevel(inputValue);
    generatedPlanet.calcTradeCodes(info.tradeCodes);
    if (display) {
        displayPlanet(generatedPlanet);
    }
    return generatedPlanet;
}
exports.generatePlanet = generatePlanet;
function createEmptyPlanet() {
    return new CreatedPlanet_1.CreatedPlanet();
}
exports.createEmptyPlanet = createEmptyPlanet;
function displayPlanet(testPlanet) {
    throw new Error("Function not implemented.");
}
//# sourceMappingURL=generalFunctions.js.map