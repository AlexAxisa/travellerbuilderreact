"use strict";
exports.__esModule = true;
var generalFunctions_1 = require("./generalFunctions");
var subSector = (function () {
    function subSector(subSectorName) {
        this.subSectorName = subSectorName;
        this.contents = new Map();
        this.width = 8;
        this.height = 10;
    }
    subSector.prototype.addPlanetToHex = function (hexNumber, createdPlanet) {
        var currentHexContent = this.contents.get(hexNumber);
        if (currentHexContent == undefined) {
            this.createHex(hexNumber);
        }
    };
    subSector.prototype.createHex = function (hexNumber) {
        var gasGiant = false;
        var gasGiantDistance = 0;
        var planets = [];
        if (generalFunctions_1.rollDice(2) < 10) {
            gasGiant = true;
            gasGiantDistance = generalFunctions_1.rollDice(1);
        }
        this.contents.set(hexNumber, { planets: planets, gasGiant: gasGiant, gasGiantDistance: gasGiantDistance });
    };
    subSector.prototype.generateSubSector = function (info) {
        for (var i = 0; i <= this.width * this.height; i++) {
            var planets = [];
            var generatedPlanet = void 0;
            var gasGiant = false;
            var gasGiantDistance = 0;
            if (generalFunctions_1.rollDice(1) < 4) {
                generatedPlanet = generalFunctions_1.generatePlanet(info, false);
                planets.push(generatedPlanet);
                if (generalFunctions_1.rollDice(2) < 10) {
                    gasGiant = true;
                    gasGiantDistance = generalFunctions_1.rollDice(1);
                }
                var diviedIteration = i / 10;
                var modulusIteration = i % 10;
                var col = Math.floor(diviedIteration);
                var row = modulusIteration * 2;
                if (diviedIteration % 2 === 1) {
                    row = row + 1;
                }
                this.contents.set({ col: col, row: row }, { planets: planets, gasGiant: gasGiant, gasGiantDistance: gasGiantDistance });
            }
        }
        console.log(this.contents);
    };
    return subSector;
}());
exports.subSector = subSector;
//# sourceMappingURL=Subsector.js.map