"use strict";
exports.__esModule = true;
var React = require("react");
var PlanetImport_1 = require("./planetBuilder/PlanetImport");
exports.PlanetContext = React.createContext(undefined);
function PlanetInformationProvider(props) {
    var planetValues = React.useState(PlanetImport_1.importPlanetInformation(props.json));
    return React.createElement(exports.PlanetContext.Provider, { value: planetValues }, props.children);
}
exports.PlanetInformationProvider = PlanetInformationProvider;
//# sourceMappingURL=planetContext.js.map