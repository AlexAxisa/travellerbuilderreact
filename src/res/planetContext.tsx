import * as React from "react";
import { importPlanetInformation } from "./planetBuilder/PlanetImport";

export const PlanetContext = React.createContext(undefined);

export function PlanetInformationProvider(props) {
  const planetValues = React.useState(importPlanetInformation(props.json));
  return <PlanetContext.Provider value={planetValues}>
      {props.children}
  </PlanetContext.Provider>;
}
