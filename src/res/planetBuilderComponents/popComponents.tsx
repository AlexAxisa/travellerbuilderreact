import * as React from "react";

export function PopWrapper() {
  return (
    <div>
      <PopMessage></PopMessage>
      <Pop></Pop>
    </div>
  );
}
function PopMessage() {
  return <div id="PopMessage"></div>;
}
function Pop() {
  return (
    <div>
      <PopForm></PopForm>
      <PopTable></PopTable>
    </div>
  );
}
function PopForm() {
  return (
    <div>
      <form>
        <input id="PopInput" type="number"></input>
        <input
          id="PopSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function PopTable() {
  return (
    <table id="PopTable">
      <tr>
        <th>Population Value</th>
        <th>Inhabitants</th>
        <th>Range</th>
        <th>Description</th>
      </tr>
    </table>
  );
}
