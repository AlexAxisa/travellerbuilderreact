import * as React from "react";

export function GovWrapper() {
  return (
    <div>
      <GovMessage></GovMessage>
      <Gov></Gov>
    </div>
  );
}
function GovMessage() {
  return <div id="GovMessage"></div>;
}
function Gov() {
  return (
    <div>
      <GovForm></GovForm>
      <GovTable></GovTable>
    </div>
  );
}
function GovForm() {
  return (
    <div>
      <form>
        <input id="GovInput" type="number"></input>
        <input
          id="GovSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function GovTable() {
  return (
    <table id="GovTable">
      <tr>
        <th>Government Value</th>
        <th>Type</th>
        <th>Description</th>
        <th>Examples</th>
        <th>Contraband</th>
      </tr>
    </table>
  );
}
