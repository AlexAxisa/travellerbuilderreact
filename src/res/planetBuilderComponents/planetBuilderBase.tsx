import * as React from "react";
import { PlanetContext } from "../planetContext";
import { AtmoWrapper } from "./atmoComponents";
import { GovWrapper } from "./govComponents";
import { HydroWrapper } from "./hydroComponents";
import { LawWrapper } from "./lawComponents";
import { PopWrapper } from "./popComponents";
import { SizeWrapper } from "./sizeComponents";
import { StarportWrapper } from "./starportComponents";
import { TempWrapper } from "./TempComponents";
import { TradeWrapper } from "./tradecodeComponents";

export function PlanetWrapper() {
  return (
    <div className="PlanetWrapper">
      <PlanetDetails></PlanetDetails>
    </div>
  );
}
function PlanetDetails() {
  return (
    <div>
      Planet Details
      <GenerateButtions></GenerateButtions>
      <CurrentPlanet></CurrentPlanet>
      <SizeWrapper></SizeWrapper>
      <AtmoWrapper></AtmoWrapper>
      <TempWrapper></TempWrapper>
      <HydroWrapper></HydroWrapper>
      <PopWrapper></PopWrapper>
      <GovWrapper></GovWrapper>
      <LawWrapper></LawWrapper>
      <StarportWrapper></StarportWrapper>
      <TradeWrapper></TradeWrapper>
    </div>
  );
}

function GenerateButtions() {
  return (
    <div>
      <GenerateSubsector></GenerateSubsector>
      <GeneratePlanetButton></GeneratePlanetButton>
      <CreateEmptyPlanet></CreateEmptyPlanet>
    </div>
  );
}

function GenerateSubsector() {
  return (
    <div>
      <button id="GenerateSubsector">Generate Subsector</button>
    </div>
  );
}
function GeneratePlanetButton() {
  return (
    <div>
      <button id="GeneratePlanetButton">Generate Planet</button>
    </div>
  );
}
function CreateEmptyPlanet() {
  return (
    <div>
      <button id="CreateEmptyPlanet">Create Empty Planet</button>
    </div>
  );
}

function CurrentPlanet() {
  return (
    <div>
      <PlanetName></PlanetName>
    </div>
  );
}
function PlanetName() {
  return (
    <div>
      Planet Name: <input type="text"></input>
    </div>
  );
}
