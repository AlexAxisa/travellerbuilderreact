import * as React from "react";

export function AtmoWrapper() {
  return (
    <div>
      <AtmoMessage></AtmoMessage>
      <Atmo></Atmo>
    </div>
  );
}
function AtmoMessage() {
  return <div id="AtmoMessage"></div>;
}
function Atmo() {
  return (
    <div>
      <AtmoForm></AtmoForm>
      <AtmoTable></AtmoTable>
    </div>
  );
}
function AtmoForm() {
  return (
    <div>
      <form>
        <input id="AtmoInput" type="number"></input>
        <input
          id="AtmoSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function AtmoTable() {
  return (
    <table id="AtmoTable">
      <tr>
        <th>Atmosphere Value</th>
        <th>Composition</th>
        <th>Examples</th>
        <th>GearRequired</th>
        <th>MinTL</th>
      </tr>
    </table>
  );
}
