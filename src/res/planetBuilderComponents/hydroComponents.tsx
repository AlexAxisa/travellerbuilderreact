import * as React from "react";

export function HydroWrapper() {
  return (
    <div>
      <HydroMessage></HydroMessage>
      <Hydro></Hydro>
    </div>
  );
}
function HydroMessage() {
  return <div id="HydroMessage"></div>;
}
function Hydro() {
  return (
    <div>
      <HydroForm></HydroForm>
      <HydroTable></HydroTable>
    </div>
  );
}
function HydroForm() {
  return (
    <div>
      <form>
        <input id="HydroInput" type="number"></input>
        <input
          id="HydroSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function HydroTable() {
  return (
    <table id="HydroTable">
      <tr>
        <th>Hydrographics Value</th>
        <th>Percentage</th>
        <th>Description</th>
      </tr>
    </table>
  );
}
