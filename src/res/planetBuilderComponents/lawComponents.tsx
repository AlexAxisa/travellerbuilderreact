import * as React from "react";

export function LawWrapper() {
  return (
    <div>
      <LawMessage></LawMessage>
      <Law></Law>
    </div>
  );
}
function LawMessage() {
  return <div id="LawMessage"></div>;
}
function Law() {
  return (
    <div>
      <LawForm></LawForm>
      <LawTable></LawTable>
    </div>
  );
}
function LawForm() {
  return (
    <div>
      <form>
        <input id="LawInput" type="number"></input>
        <input
          id="LawSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function LawTable() {
  return (
    <table id="LawTable">
      <tr>
        <th>Law Value</th>
        <th>Weapons</th>
        <th>Armour</th>
      </tr>
    </table>
  );
}
