import * as React from "react";
import { PlanetContext } from "../planetContext";

export function SizeWrapper() {
  return (
    <div>
      <SizeMessage></SizeMessage>
      <Size></Size>
    </div>
  );
}
function SizeMessage() {
  return <div id="SizeMessage"></div>;
}
function Size() {
  return (
    <div>
      <SizeForm></SizeForm>
      <SizeTable></SizeTable>
    </div>
  );
}
function SizeForm() {
  return (
    <div>
      <form>
        <input id="SizeInput" type="number"></input>
        <input
          id="SizeSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function SizeTable() {
  return (
    <table id="SizeTable">
      <tr>
        <th>Size Value</th>
        <th>Diameter</th>
        <th>Example</th>
        <th>Surface Gravity</th>
      </tr>
      <tr>
        <SizeTableEntriesTesting></SizeTableEntriesTesting>
      </tr>
    </table>
  );
}

function SizeTableEntriesTesting() {
  const [context, setcontext] = React.useContext(PlanetContext);
  let rows = [];
  for (let i = 0; i < context.sizes.size; i++) {
    const row = <tr>{context.sizes.get(i).example}</tr>
    rows.push(row);
  }
  return <React.Fragment>{rows}</React.Fragment>;
}
