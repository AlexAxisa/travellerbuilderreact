import * as React from "react";

export function TradeWrapper() {
  return (
    <div>
      <TradeMessage></TradeMessage>
      <Trade></Trade>
    </div>
  );
}
function TradeMessage() {
  return <div id="TradeMessage"></div>;
}
function Trade() {
  return (
    <div>
      <TradeForm></TradeForm>
      <TradeTable></TradeTable>
    </div>
  );
}
function TradeForm() {
  return (
    <div>
      <form>
        <input id="TradeInput" type="number"></input>
        <input
          id="TradeSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function TradeTable() {
  return (
    <table id="TradeTable">
      <tr>
        <th>Classification</th>
        <th>Code</th>
      </tr>
    </table>
  );
}
