import * as React from "react";

export function StarportWrapper() {
  return (
    <div>
      <StarportMessage></StarportMessage>
      <Starport></Starport>
    </div>
  );
}
function StarportMessage() {
  return <div id="StarportMessage"></div>;
}
function Starport() {
  return (
    <div>
      <StarportForm></StarportForm>
      <StarportTable></StarportTable>
    </div>
  );
}
function StarportForm() {
  return (
    <div>
      <form>
        <input id="StarportInput" type="number"></input>
        <input
          id="StarportSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function StarportTable() {
  return (
    <table id="StarportTable">
      <tr>
        <th>Class</th>
        <th>Quality</th>
        <th>Berthing</th>
        <th>Fuel</th>
        <th>Facilities</th>
        <th>Naval</th>
        <th>Scout</th>
        <th>Research</th>
        <th>Tas</th>
      </tr>
    </table>
  );
}
