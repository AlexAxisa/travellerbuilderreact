import * as React from "react";

export function TempWrapper() {
  return (
    <div>
      <TempMessage></TempMessage>
      <Temp></Temp>
    </div>
  );
}
function TempMessage() {
  return <div id="TempMessage"></div>;
}
function Temp() {
  return (
    <div>
      <TempForm></TempForm>
      <TempTable></TempTable>
    </div>
  );
}
function TempForm() {
  return (
    <div>
      <form>
        <input id="TempInput" type="number"></input>
        <input
          id="TempSubmitButton"
          type="submit"
          value="Submit"
          disabled
        ></input>
      </form>
    </div>
  );
}

function TempTable() {
  return (
    <table id="TempTable">
      <tr>
        <th>Temperature Value</th>
        <th>Type</th>
        <th>Average Tempature</th>
        <th>Description</th>
      </tr>
    </table>
  );
}
