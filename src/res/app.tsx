import * as React from "react";
import * as ReactDOM from "react-dom";
import "./index.css";
import * as PlanetComponents from "./planetBuilderComponents/planetBuilderBase";
import { PlanetInformationProvider } from "./planetContext";

fetch("http://localhost:3000/PlanetInformation.json")
  .then((response) => response.json())
  .then((data) => {
    ReactDOM.render(
        <PlanetInformationProvider json={data}>
        <PlanetComponents.PlanetWrapper />
      </PlanetInformationProvider>,
      document.getElementById("root")
    );
  });