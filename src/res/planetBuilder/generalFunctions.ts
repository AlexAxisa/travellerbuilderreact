import { CreatedPlanet } from "./CreatedPlanet";
import { AllPlanetInfo } from "./PlanetImport";

export function rollDice(dice: number) {
  let totalroll = 0;
  for (let i = 0; i < dice; i++) {
    totalroll += Math.floor(Math.random() * Math.floor(6)) + 1;
  }
  return totalroll;
}
export function generatePlanet(info: AllPlanetInfo, display = true) {
  let inputValue = undefined;
  let generatedPlanet = new CreatedPlanet();
  generatedPlanet.setPlanetSize(inputValue, info.sizes, info.ranges);
  generatedPlanet.setPlanetAtmosphere(
    inputValue,
    info.atmospheres,
    info.ranges
  );
  generatedPlanet.setPlanetTemperature(
    inputValue,
    info.temperatures,
    info.ranges
  );
  generatedPlanet.setPlanetHydro(inputValue, info.hydrographics, info.ranges);
  generatedPlanet.setPlanetPopulation(
    inputValue,
    info.populations,
    info.ranges
  );
  generatedPlanet.setPlanetGovernment(
    inputValue,
    info.governments,
    info.ranges
  );
  generatedPlanet.setPlanetLaw(inputValue, info.lawLevels, info.ranges);
  generatedPlanet.setPlanetStarport(
    inputValue,
    info.starportClasses,
    info.ranges
  );
  generatedPlanet.setStarportDetails(info.starportFacilities);
  generatedPlanet.setTechLevel(inputValue);
  generatedPlanet.calcTradeCodes(info.tradeCodes);
  if (display) {
    displayPlanet(generatedPlanet);
  }
  return generatedPlanet;
}
export function createEmptyPlanet() {
  //$("#SizeSubmitButton").prop("disabled", false);
  return new CreatedPlanet();
}
function displayPlanet(testPlanet: CreatedPlanet) {
  throw new Error("Function not implemented.");
}
