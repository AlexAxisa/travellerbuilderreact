import { rollDice } from "./generalFunctions";
import {
  Atmospheres,
  Governments,
  Hydrographics,
  LawLevels,
  PlanetRanges,
  PlanetSizes,
  Populations,
  StarportClasses,
  StarportDetails,
  StarportFacilities,
  Temperatures,
  TradeCodes,
} from "./PlanetImport";

export class CreatedPlanet {
  id:number|undefined;
  sizeRoll: number | undefined;
  sizeValue: PlanetSizes | undefined;
  atmoRoll: number | undefined;
  atmoValue: Atmospheres | undefined;
  tempRoll: number | undefined;
  tempValue: Temperatures | undefined;
  hydroRoll: number | undefined;
  hydroValue: Hydrographics | undefined;
  popRoll: number | undefined;
  popValue: Populations | undefined;
  govRoll: number | undefined;
  govValue: Governments | undefined;
  lawRoll: number | undefined;
  lawValue: LawLevels | undefined;
  classRoll: number | undefined;
  classValue: StarportClasses | undefined;
  starportDetails: StarportDetails | undefined;
  techLevel: number | undefined;
  tradeCodes: TradeCodes[];
  constructor(sizeRoll = undefined) {
    this.sizeRoll = undefined;
    this.sizeValue = undefined;
    this.atmoRoll = undefined;
    this.atmoValue = undefined;
    this.tempRoll = undefined;
    this.tempValue = undefined;
    this.hydroRoll = undefined;
    this.hydroValue = undefined;
    this.popRoll = undefined;
    this.popValue = undefined;
    this.lawRoll = undefined;
    this.lawValue = undefined;
    this.classRoll = undefined;
    this.classValue = undefined;
    this.starportDetails = undefined;
    this.techLevel = undefined;
    this.tradeCodes = [];
  }

  setPlanetSize(
    value: number | undefined,
    sizes: Map<number, PlanetSizes>,
    ranges: PlanetRanges
  ) {
    if (typeof value === "undefined") {
      let roll = rollDice(2) - 2;
      if (roll < ranges.minSize) {
        roll = ranges.minSize;
      } else if (roll > ranges.maxSize) {
        roll = ranges.maxSize;
      }
      value = roll;
    }
    this.sizeRoll = value;
    this.sizeValue = sizes.get(value);
  }

  setPlanetAtmosphere(
    value: number | undefined,
    atmospheres: Map<number, Atmospheres>,
    ranges: PlanetRanges
  ) {
    if (typeof value === "undefined") {
      let roll = rollDice(2) - 7 + this.sizeRoll;
      if (roll < ranges.minAtmo) {
        roll = ranges.minAtmo;
      } else if (roll > ranges.maxAtmo) {
        roll = ranges.maxAtmo;
      }
      value = roll;
    }
    this.atmoRoll = value;
    this.atmoValue = atmospheres.get(value);
  }

  setPlanetTemperature(
    value: number | undefined,
    temperatures: Map<number, Temperatures>,
    ranges: PlanetRanges
  ) {
    if (typeof value === "undefined") {
      let roll = rollDice(2);
      //Game Hard Coded Modifiers
      switch (this.atmoRoll) {
        case 0:
        case 1:
          roll = 0;
          break;
        case 2:
        case 3:
          roll = roll - 2;
          break;
        case 4:
        case 5:
        case 14:
          roll = roll - 1;
          break;
        case 8:
        case 9:
          roll = roll + 1;
          break;
        case 10:
        case 13:
        case 15:
          roll = roll + 2;
          break;
        case 11:
        case 12:
          roll = roll + 6;
        default:
          break;
      }
      if (roll < ranges.minTemp) {
        roll = ranges.minTemp;
      } else if (roll > ranges.maxTemp) {
        roll = ranges.maxTemp;
      }
      value = roll;
    }
    this.tempRoll = value;
    let currentSegment: number | undefined = undefined;
    temperatures.forEach((mapValue, key) => {
      if (
        value <= key &&
        (currentSegment > key || typeof currentSegment == "undefined")
      ) {
        currentSegment = key;
      }
    });
    this.tempValue = temperatures.get(currentSegment);
  }

  setPlanetHydro(
    value: number | undefined,
    hydrographics: Map<number, Hydrographics>,
    ranges: PlanetRanges
  ) {
    if (typeof value === "undefined") {
      let roll = rollDice(2) - 7;

      if (this.sizeRoll == 1 || this.sizeRoll == 2) {
        roll = 0;
      } else if (
        this.atmoRoll == 0 ||
        this.atmoRoll == 1 ||
        this.atmoRoll == 10 ||
        this.atmoRoll == 11 ||
        this.atmoRoll == 12
      ) {
        roll = roll - 4;
      } else {
        roll = roll + this.atmoRoll;
      }
      if (this.atmoRoll !== 13 && this.atmoRoll !== 15) {
        if (this.tempRoll == 11) {
          roll = roll - 2;
        } else if (this.tempRoll == 12) {
          roll = roll - 6;
        }
      }
      if (roll < ranges.minHydro) {
        roll = ranges.minHydro;
      } else if (roll > ranges.maxHydro) {
        roll = ranges.maxHydro;
      }
      value = roll;
    }
    this.hydroRoll = value;
    this.hydroValue = hydrographics.get(value);
  }
  setPlanetPopulation(
    value: number | undefined,
    population: Map<number, Populations>,
    ranges: PlanetRanges
  ) {
    if (typeof value === "undefined") {
      let roll = rollDice(2) - 2;
      if (roll < ranges.minPop) {
        roll = ranges.minPop;
      } else if (roll > ranges.maxPop) {
        roll = ranges.maxPop;
      }
      value = roll;
    }
    this.popRoll = value;
    this.popValue = population.get(value);
  }
  setPlanetGovernment(
    value: number | undefined,
    governments: Map<number, Governments>,
    ranges: PlanetRanges
  ) {
    if (typeof value === "undefined") {
      let roll = rollDice(2) - 7 + this.popRoll;
      if (this.popRoll === 0) {
        roll = 0;
      }
      if (roll < ranges.minGov) {
        roll = ranges.minGov;
      } else if (roll > ranges.maxGov) {
        roll = ranges.maxGov;
      }
      value = roll;
    }
    this.govRoll = value;
    this.govValue = governments.get(value);
  }

  setPlanetFaction() {}

  setPlanetLaw(
    value: number | undefined,
    laws: Map<number, LawLevels>,
    ranges: PlanetRanges
  ) {
    if (typeof value === "undefined") {
      let roll = rollDice(2) - 7 + this.govRoll;
      if (this.popRoll === 0) {
        roll = 0;
      }
      if (roll < ranges.minLaw) {
        roll = ranges.minLaw;
      } else if (roll > ranges.maxLaw) {
        roll = ranges.maxLaw;
      }
      value = roll;
    }
    this.lawRoll = value;
    this.lawValue = laws.get(value);
  }

  setPlanetStarport(
    value: number | undefined,
    starport: Map<number, StarportClasses>,
    ranges: PlanetRanges
  ) {
    if (typeof value === "undefined") {
      let roll = rollDice(2);
      if (this.popRoll > 10) {
        roll = roll + 2;
      } else if (this.popRoll > 8) {
        roll = roll + 1;
      } else if (this.popRoll < 2) {
        roll = roll - 2;
      } else if (this.popRoll < 4) {
        roll = roll - 1;
      }

      if (roll < ranges.minClass) {
        roll = ranges.minClass;
      } else if (roll > ranges.maxClass) {
        roll = ranges.maxClass;
      }
      value = roll;
    }
    this.classRoll = value;
    this.classValue = starport.get(value);
  }
  setStarportDetails(starportFacilities: Map<string, StarportFacilities>) {
    let starportConstuctor = starportFacilities.get(this.classValue.class);
    let berthingCost = rollDice(1) * starportConstuctor.berthing;
    let naval: boolean = rollDice(2) > starportConstuctor.naval;
    let scout: boolean = rollDice(2) > starportConstuctor.scout;
    let research: boolean = rollDice(2) > starportConstuctor.research;
    let tas: boolean = rollDice(2) > starportConstuctor.TAS;
    this.starportDetails = {
      TLMod: starportConstuctor.TLMod,
      quality: starportConstuctor.quality,
      facilities: starportConstuctor.facilities,
      fuel: starportConstuctor.fuel,
      berthingCost,
      naval,
      scout,
      research,
      TAS: tas,
    };
  }
  setTechLevel(value: number | undefined) {
    if (typeof value === "undefined") {
      let roll = rollDice(1);
      roll += this.starportDetails.TLMod;
      roll += this.sizeValue.TLMod;
      roll += this.atmoValue.TLMod;
      roll += this.hydroValue.TLMod;
      roll += this.popValue.TLMod;
      roll += this.govValue.TLMod;
      if (this.popRoll === 0) {
        roll = 0;
      }
      value = roll;
    }
    this.techLevel = value;
  }
  calcTradeCodes(codes: TradeCodes[]) {
    this.tradeCodes = [];
    codes.forEach((nextCode) => {
      let planetIsValid = chechValidCodeParamter(
        nextCode.planetSizes,
        this.sizeRoll
      );
      let atmoIsValid = chechValidCodeParamter(
        nextCode.atmospheres,
        this.atmoRoll
      );
      let hydroIsValid = chechValidCodeParamter(nextCode.hydro, this.hydroRoll);
      let popIsValid = chechValidCodeParamter(
        nextCode.population,
        this.popRoll
      );
      let lawIsValid = chechValidCodeParamter(nextCode.lawLevels, this.lawRoll);
      let TechisValid = chechValidCodeParamter(
        nextCode.techLevels,
        this.techLevel
      );
      if (
        planetIsValid &&
        atmoIsValid &&
        hydroIsValid &&
        popIsValid &&
        lawIsValid &&
        TechisValid
      ) {
        this.tradeCodes.push(nextCode);
      }
    });
  }
}

function chechValidCodeParamter(
  possibleOptions: number[],
  verifiedParamater: number
): boolean {
  if (possibleOptions.length !== 0) {
    let isValid = false;
    possibleOptions.forEach((value) => {
      if (value == verifiedParamater) {
        isValid = true;
      }
    });
    return isValid;
  } else {
    return true;
  }
}
