import { CreatedPlanet } from "./CreatedPlanet";
import { generatePlanet, rollDice } from "./generalFunctions";
import { AllPlanetInfo } from "./PlanetImport";
//each Subsector is 8x10;
//density modifier
export class subSector {
  subSectorName: string;
  contents: Map<coordnates, hexDetails>;
  width: number;
  height: number;
  constructor(subSectorName) {
    this.subSectorName = subSectorName;
    this.contents = new Map<coordnates, hexDetails>();
    this.width = 8;
    this.height = 10;
  }

  addPlanetToHex(hexNumber: coordnates, createdPlanet: CreatedPlanet) {
    let currentHexContent = this.contents.get(hexNumber);
    if (currentHexContent == undefined) {
      this.createHex(hexNumber);
    }
  }
  createHex(hexNumber: coordnates) {
    let gasGiant = false;
    let gasGiantDistance = 0;
    let planets: CreatedPlanet[] = [];
    if (rollDice(2) < 10) {
      gasGiant = true;
      gasGiantDistance = rollDice(1);
    }
    this.contents.set(hexNumber, { planets, gasGiant, gasGiantDistance });
  }

  generateSubSector(info: AllPlanetInfo) {
    for (let i = 0; i <= this.width * this.height; i++) {
      let planets:CreatedPlanet[] = []
      let generatedPlanet: CreatedPlanet;
      let gasGiant: boolean = false;
      let gasGiantDistance: number = 0;
      if (rollDice(1) < 4) {
        generatedPlanet = generatePlanet(info, false);
        planets.push(generatedPlanet);
        if (rollDice(2) < 10) {
          gasGiant = true;
          gasGiantDistance = rollDice(1);
        }
        let diviedIteration = i / 10;
        let modulusIteration = i % 10;
        let col = Math.floor(diviedIteration);
        let row = modulusIteration * 2;
        if (diviedIteration % 2 === 1) {
          row = row + 1;
        }
        this.contents.set({col,row}, {planets, gasGiant, gasGiantDistance});
      }
    }
    console.log(this.contents);
  }
}

type coordnates = { col: number; row: number };
export type hexDetails = {
  planets: CreatedPlanet[];
  gasGiant: boolean;
  gasGiantDistance: number;
};
