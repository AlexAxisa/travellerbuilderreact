export function importPlanetInformation(json: any) {
  let sizes: Map<number, PlanetSizes> = new Map();
  let minSize = Number.POSITIVE_INFINITY;
  let maxSize = Number.NEGATIVE_INFINITY;
  json.Size.forEach((currentSize) => {
    if (currentSize.Size < minSize) {
      minSize = currentSize.Size;
    }
    if (currentSize.Size > maxSize) {
      maxSize = currentSize.Size;
    }
    let nextSize: PlanetSizes = {
      diameter: currentSize.Diameter,
      example: currentSize.Example,
      surfaceGravity: currentSize.SurfaceGravity,
      TLMod: currentSize.TLMod,
    };
    sizes.set(currentSize.Size, nextSize);
  });

  let atmospheres: Map<number, Atmospheres> = new Map();
  let minAtmo = Number.POSITIVE_INFINITY;
  let maxAtmo = Number.NEGATIVE_INFINITY;
  json.Atmosphere.forEach((currentAthmo) => {
    if (currentAthmo.Atmosphere < minAtmo) {
      minAtmo = currentAthmo.Atmosphere;
    }
    if (currentAthmo.Atmosphere > maxAtmo) {
      maxAtmo = currentAthmo.Atmosphere;
    }
    let nextAthmo: Atmospheres = {
      composition: currentAthmo.Composition,
      example: currentAthmo.Examples,
      pressureMin: currentAthmo.PressureMin,
      pressureMax: currentAthmo.PressureMax,
      gearRequired: currentAthmo.GearRequired,
      minTL: currentAthmo.MinTL,
      TLMod: currentAthmo.TLMod,
    };
    atmospheres.set(currentAthmo.Atmosphere, nextAthmo);
  });

  let temperatures: Map<number, Temperatures> = new Map();
  let minTemp = Number.POSITIVE_INFINITY;
  let maxTemp = Number.NEGATIVE_INFINITY;
  json.Temperature.forEach((currentTemp) => {
    if (currentTemp.Roll < minTemp) {
      minTemp = currentTemp.Roll;
    }
    if (currentTemp.Roll > maxTemp) {
      maxTemp = currentTemp.Roll;
    }
    let nextTemp: Temperatures = {
      type: currentTemp.Type,
      averageTemp: currentTemp.AverageTemp,
      description: currentTemp.Description,
    };
    temperatures.set(currentTemp.Roll, nextTemp);
  });

  let hydrographics: Map<number, Hydrographics> = new Map();
  let minHydro = Number.POSITIVE_INFINITY;
  let maxHydro = Number.NEGATIVE_INFINITY;
  json.Hydrographics.forEach((currentHydro) => {
    if (currentHydro.Hydrographics < minHydro) {
      minHydro = currentHydro.Hydrographics;
    }
    if (currentHydro.Hydrographics > maxHydro) {
      maxHydro = currentHydro.Hydrographics;
    }
    let nextHydro: Hydrographics = {
      percentage: currentHydro.Percentage,
      description: currentHydro.Description,
      TLMod: currentHydro.TLMod,
    };
    hydrographics.set(currentHydro.Hydrographics, nextHydro);
  });

  let populations: Map<number, Populations> = new Map();
  let minPop = Number.POSITIVE_INFINITY;
  let maxPop = Number.NEGATIVE_INFINITY;
  json.Population.forEach((currentPop) => {
    if (currentPop.Population < minPop) {
      minPop = currentPop.Population;
    }
    if (currentPop.Population > maxPop) {
      maxPop = currentPop.Population;
    }
    let nextPop: Populations = {
      inhabitants: currentPop.Inhabitants,
      range: currentPop.Range,
      description: currentPop.Description,
      TLMod: currentPop.TLMod,
    };
    populations.set(currentPop.Population, nextPop);
  });

  let governments: Map<number, Governments> = new Map();
  let minGov = Number.POSITIVE_INFINITY;
  let maxGov = Number.NEGATIVE_INFINITY;
  json.Government.forEach((currentGov) => {
    if (currentGov.Government < minGov) {
      minGov = currentGov.Government;
    }
    if (currentGov.Government > maxGov) {
      maxGov = currentGov.Government;
    }
    let nextGov: Governments = {
      TLMod: currentGov.TLMod,
      type: currentGov.Type,
      description: currentGov.Description,
      examples: currentGov.Examples,
      contraband: currentGov.Contraband,
    };
    governments.set(currentGov.Government, nextGov);
  });

  let lawLevels: Map<number, LawLevels> = new Map();
  let minLaw = Number.POSITIVE_INFINITY;
  let maxLaw = Number.NEGATIVE_INFINITY;
  json.LawLevel.forEach((currentLaw) => {
    if (currentLaw.Law < minLaw) {
      minLaw = currentLaw.Law;
    }
    if (currentLaw.Law > maxLaw) {
      maxLaw = currentLaw.Law;
    }
    let nextLaw: LawLevels = {
      weapons: currentLaw.Weapons,
      armor: currentLaw.Armour,
    };
    lawLevels.set(currentLaw.Law, nextLaw);
  });

  let starportClasses: Map<number, StarportClasses> = new Map();
  let minClass = Number.POSITIVE_INFINITY;
  let maxClass = Number.NEGATIVE_INFINITY;
  json.StarportClass.forEach((currentStarport) => {
    if (currentStarport.Roll < minClass) {
      minClass = currentStarport.Roll;
    }
    if (currentStarport.Roll > maxClass) {
      maxClass = currentStarport.Roll;
    }
    let nextStarport: StarportClasses = {
      class: currentStarport.Class,
    };
    starportClasses.set(currentStarport.Roll, nextStarport);
  });
  let starportFacilities: Map<string, StarportFacilities> = new Map();
  json.StarportFacilities.forEach((currentStarFac) => {
    let nextStarFac: StarportFacilities = {
      TLMod: currentStarFac.TLMod,
      quality: currentStarFac.Quality,
      berthing: currentStarFac.Berthing,
      fuel: currentStarFac.Fuel,
      facilities: currentStarFac.Facilities,
      naval: currentStarFac.Naval,
      scout: currentStarFac.Scout,
      research: currentStarFac.Research,
      TAS: currentStarFac.Tas,
    };
    starportFacilities.set(currentStarFac.Class, nextStarFac);
  });
  let tradeCodes: TradeCodes[] = [];
  json.TradeCodes.forEach((currentCode) => {
    let nextCode: TradeCodes = {
      classification: currentCode.Classification,
      code: currentCode.Code,
      planetSizes: currentCode.PlanetSize,
      atmospheres: currentCode.Atmosphere,
      hydro: currentCode.Hydro,
      population: currentCode.Population,
      governments: currentCode.Government,
      lawLevels: currentCode.LawLevel,
      techLevels: currentCode.TechLevel,
    };
    tradeCodes.push(nextCode);
  });
  let ranges: PlanetRanges = {
    minSize,
    maxSize,
    minAtmo,
    maxAtmo,
    minTemp,
    maxTemp,
    minHydro,
    maxHydro,
    minPop,
    maxPop,
    minGov,
    maxGov,
    minLaw,
    maxLaw,
    minClass,
    maxClass,
  };
  let allInfo: AllPlanetInfo = {
    sizes,
    atmospheres,
    temperatures,
    hydrographics,
    populations,
    governments,
    lawLevels,
    starportClasses,
    starportFacilities,
    tradeCodes,
    ranges,
  };
  return allInfo;
}

export type AllPlanetInfo = {
  sizes: Map<number, PlanetSizes>;
  atmospheres: Map<number, Atmospheres>;
  temperatures: Map<number, Temperatures>;
  hydrographics: Map<number, Hydrographics>;
  populations: Map<number, Populations>;
  governments: Map<number, Governments>;
  lawLevels: Map<number, LawLevels>;
  starportClasses: Map<number, StarportClasses>;
  starportFacilities: Map<string, StarportFacilities>;
  tradeCodes: TradeCodes[];
  ranges: PlanetRanges;
};
export type PlanetRanges = {
  minSize: number;
  maxSize: number;
  minAtmo: number;
  maxAtmo: number;
  minTemp: number;
  maxTemp: number;
  minHydro: number;
  maxHydro: number;
  minPop: number;
  maxPop: number;
  minGov: number;
  maxGov: number;
  minLaw: number;
  maxLaw: number;
  minClass: number;
  maxClass: number;
};
export type PlanetSizes = {
  diameter: string;
  example: string;
  surfaceGravity: number;
  TLMod: number;
};
export type Atmospheres = {
  composition: string;
  example: string;
  pressureMin: number;
  pressureMax: number;
  gearRequired: string;
  minTL: number;
  TLMod: number;
};
export type Temperatures = { type: string; averageTemp: string; description: string };
export type Hydrographics = { percentage: string; description: string; TLMod: number };
export type Populations = {
  inhabitants: string;
  range: number;
  description: string;
  TLMod: number;
};
export type Governments = {
  TLMod: number;
  type: string;
  description: string;
  examples: string;
  contraband: string;
};
export type LawLevels = { weapons: string; armor: string };
export type StarportClasses = { class: string };
export type StarportFacilities = {
  TLMod: number;
  quality: string;
  berthing: number;
  fuel: string;
  facilities: string;
  naval: number;
  scout: number;
  research: number;
  TAS: number;
};
export type TradeCodes = {
  classification: string;
  code: string;
  planetSizes: number[];
  atmospheres: number[];
  hydro: number[];
  population: number[];
  governments: number[];
  lawLevels: number[];
  techLevels: number[];
};
export type StarportDetails = {
  TLMod: number;
  quality: string;
  fuel: string;
  facilities: string;
  berthingCost: number;
  naval: boolean;
  scout: boolean;
  research: boolean;
  TAS: boolean;
};
