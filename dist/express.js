const express = require('express');
const app = express();
const port = 3000;

app.use(express.static(__dirname+'/res'));

//app.get('/css', (req, res) => {res.sendFile(__dirname + "/res/index.css")})
app.get('/', (req, res) => {res.sendFile(__dirname + "/res/app.html")});
app.get('/PlanetInfo', (req, res) => {res.sendFile(__dirname + "/res/PlanetInformation.json")});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));