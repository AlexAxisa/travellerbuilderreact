const path = require("path");

module.exports = {
    entry: { index: path.resolve(__dirname, "src/compiled/", "app.js") },
    output: {
        path: path.resolve(__dirname, "dist/res"),
        filename: 'app.js'
      }
  };